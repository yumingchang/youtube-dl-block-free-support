This is my first simple implementation for downloading with youtube-dl 

* target
	- automatic trigger youtube-dl on background
	- non-blocking use of youtube-dl( so a queue to store urls )
		- because youtube-dl takes over the console when downloading, that is fast, however if using auto-transcode function in youtube-dl, that will require a tmux or a terminal dedicated for a long time ( i am running this on a Rock64 )

* scripts usage
	- auto-youtube-dl : it takes a line of url from the file based queue( here i used /home/markchang/.youtube-dl.todownload ), and pass it to youtube-dl, if successfully downloaded, the line will be removed
	- download        : the script to add urls to file based queue, usage is
		- download "URL" "URL" ...
	- crontab.txt     : an example for setting up crontab, this is used to check update at 5am each day
	- youtube-dl.conf : just an example of my youtube-dl config
	- the incrontab is remove since it is more easy to trigger the download from "download" script

* requirements
	- [flock](http://man7.org/linux/man-pages/man2/flock.2.html)
	- wget
	- python 2.7 / 3.2+
	- cron deamon
